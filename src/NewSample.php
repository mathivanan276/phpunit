<?php

namespace App;

class NewSample
{
    public function subtract(int $a, int $b): int
    {
        return $a-$b;
    }
}