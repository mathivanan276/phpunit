<?php

use PHPUnit\Framework\TestCase;
use App\Sample;
use App\NewSample;

$GLOBALS["foo"] = "FOO";
/*
* @backupGlobals enabled
* @backupStaticAttributes disabled
*/
class SampleTest extends TestCase
{
    private $var;

    protected function setUp(): void
    {
        $this->var = ["world"];
    }

    protected function tearDown(): void
    {
        $this->var = [];
    }
    
    /**
    * @backupStaticAttributes disabled
    * @covers \App\Sample
    * @covers \App\NewSample
    * @requires PHPUnit >= 9.5
    */
    public function testAddTwoNumber(): Sample
    {
        $sample = new Sample();
        $sum = $sample->add(1,2);
        $this->assertSame(3, $sum);
        $newSample = new NewSample();
        $sub = $newSample->subtract(10,5);
        $this->assertSame(5, $sub);
        return $sample;
    }
    /**
     * @depends testAddTwoNumber
     * @backupStaticAttributes enabled
     */
    public function testDependency(Sample $sample):int
    {
        array_push($this->var, "hello");
        $this->assertSame(["world", "hello"], $this->var);
        if($sample instanceof NewSample){
            $this->markTestSkipped();
        }
        $this->expectOutputString("3 is the sum of given two numbers");
        $this->assertSame("3 is the sum of given two numbers", $sample->getResultString());
        return $sample->getResult();
    }

    /**
    *@depends testAddTwoNumber
    *@depends testDependency
    */
    public function testMoreThanOneDependency(Sample $sample, int $secondFixture): void
    {
        // $this->assertSame(["world", "hello"], $this->var);  // fails because the the value is reset in tearDown()
        $this->assertSame($sample->getResult(), $secondFixture);
    }

    /**
     *@dataProvider firstProvider
     */
    public function testFirstDataProviders(int $a, int $b): void
    {
        $this->assertSame([1,2], array($a,$b));
    }

    /**
     * @dataProvider secondProvider
     */
    public function testSecondDataProvider(int $a, int $b): void
    {
        $this->assertSame([3,4], array($a, $b));
    }

    public function firstProvider(): array
    {
        return [
            [1,2]
            // "three and four" => [3,4]
        ];
    }

    public function secondProvider(): array
    {
        return [
            [3,4]
        ];
    }
    
    public function testException(): void
    {
        $this->expectExceptionMessageMatches("/invalid/");
        $sample = new Sample();
        $sample->add(1,5);
    }

    public function testErrorCanBeExpected(): void
    {
        $sample = new Sample();
        $this->expectException(ArgumentCountError::class);
        $sample->add();
    }

    /** 
    * @backupGlobals enabled
    */
    public function testGlobalStateWithOutBackup(): void
    {
        $this->assertSame("FOO", $GLOBALS["foo"]);
        $GLOBALS["foo"] = "BAR";
    }

    /**
     * @backupGlobals enabled
     */
    public function testGlobalStateWithOutBackuptest(): void
    {
        $this->assertSame("FOO", $GLOBALS["foo"]);
    }

    /**
     * @backupStaticAttributes enabled
     */
    public function testStaticVariableBackup(): void
    {
        $this->assertSame("static var", Sample::$static);
        Sample::$static = "varied";
    }

    /**
     *@backupStaticAttributes enabled
     * @return void
     */
    public function testStaticVariableBackupTest(): void
    {
        $this->assertSame("static var", Sample::$static);
    }

    public function testTestThatDoNothing(): void
    {
        $this->markTestIncomplete('This test has not been implemented yet.');
    }
}