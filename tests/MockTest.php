<?php

use PHPUnit\Framework\TestCase;
use App\MockSample;
use App\Sample;

final class MockTest extends TestCase
{
    public function testMockObjectCreationUsingStub(): void
    {
        $stub = $this->createStub(MockSample::class);

        $stub->method('mockMethod')
            ->willReturn(true);

        $stubClone = clone $stub;

        $this->assertTrue($stub->mockMethod());
        // $this->assertSame('clone', $stubClone->varClone); // fails because stubs does not execute __clone
        // $this->assertSame('construct', $stub->var); // fails because stubs does not execute __construct method
    }

    public function testMockObjectCreationUsingBuilder(): void
    {
        $stub = $this->getMockBuilder(MockSample::class)->getMock();

        $stub
            ->method('mockMethod')
            ->willReturn(true);

        $stubClone = clone $stub;

        $this->assertTrue($stub->mockMethod());
        $this->assertSame('clone', $stubClone->varClone);
        $this->assertSame('construct', $stub->var);
    }

    public function testReturnValueMap(): void
    {
        $sample = $this->createStub(Sample::class);
        $map = [
            [1,2, 12],
            [2,3, 23]
        ];
        $sample->method('add')
            ->will($this->returnValueMap($map));
        $this->assertSame(12,$sample->add(1,2));// return  the value that specified earlier
        $this->assertSame(23,$sample->add(2,3));
    }

    public function testMockObjectExpectation(): void
    {
        $sample = $this->createMock(Sample::class);

        $sample->expects($this->exactly(3))
            ->method('add')
            ->with($this->greaterThan(5), $this->greaterThan(5))
            ->willReturn(10);
        $mock = new MockSample();

        $mock->addMockObject($sample);
        $mock->addMockObject($sample);

        $this->assertSame(15, $sample->add(7,6));

        $mock->callObjMethod();
    }
}