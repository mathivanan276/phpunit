<?php

namespace App;

class MockSample
{
    public $var;

    public $varClone;

    private $obj = [];

    public function __construct()
    {
        $this->var = "construct";
    }

    public function __clone()
    {
        $this->varClone = "clone";
    }

    public function mockMethod(): bool
    {
        return true;
    }

    public function addMockObject(Sample $sample): void
    {
        array_push($this->obj, $sample);
    }

    public function callObjMethod(): void
    {
        foreach($this->obj as $o){
            $o->add(6,7);
        }
    }
}