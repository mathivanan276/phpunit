<?php

namespace App;

class Sample 
{
    private $result;

    public static $static = "static var";

    public function add(int $a, int $b): int
    {
        if($b === 5) {
            throw new \InvalidArgumentException("invalid args");
        }
        $this->result = $a+$b;
        return $this->result;
    }

    public function getResultString(): string
    {
        $line = sprintf("%d is the sum of given two numbers", $this->result);
        print($line);
        return sprintf($line);
    }

    public function getResult(): int
    {
        return $this->result;
    }
}